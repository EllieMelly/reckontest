﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reckon
{
    public class BossHog
    {
        /// <summary>
        /// Returns:
        /// "Boss" if the number is multiple of 3,
        /// "Hog" if the number is multiple of 5,
        /// "BossHog" if the number is multiple of both 3 and 5,
        /// for any ther number return the numbe ritself
        /// </summary>
        /// <param name="number">the integer to get output for</param>
        /// <returns>a string with the proper output as described in the summary</returns>
        public static string GetValue(int number)
        {
            string output;

            if ((number % 3 == 0) && (number % 5 == 0))
            {
                output = "BossHog";
            }
            else if (number % 3 == 0)
            {
                output = "Boss";
            }
            else if (number % 5 == 0)
            {
                output = "Hog";
            }
            else
            {
                output = number.ToString();
            }

            return output;
        }
    }
}
