﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reckon
{
    class Program
    {
        static void Main(string[] args)
        {
            // search string
            var stringToSearch = "Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out. Phew!";
            var subText = "Pi";
            Console.WriteLine($"String to search => {stringToSearch}");
            Console.WriteLine($"Sub text => {subText}");
            Console.WriteLine($"occurrence indexes => {SearchString.Find(stringToSearch, subText)}");


            // BossHug, print numbers from 1 to 100.
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine(BossHog.GetValue(i));
            }

            Console.WriteLine("Press enter to quit...");
            Console.ReadLine();

        }
    }
}
