﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reckon
{
    public static class SearchString
    {
        public static string Find(string textToSearch, string subText)
        {
            if (string.IsNullOrEmpty(textToSearch) || string.IsNullOrEmpty(subText)) return "";

            textToSearch = textToSearch.ToLower();
            subText = subText.ToLower();

            int index = 0;
            string res="";

            do
            {
                index = strStr(textToSearch, subText, index);
                if (index == -1) break;
                res += (index+1).ToString() + ", ";
                index++;
            }
            while (true);

            return string.IsNullOrEmpty(res) ? "<No Output>" : res.Substring(0,res.Length-2);
        }

        private static int strStr(String haystack, String needle,int startIndex)
        {
            for (int i = startIndex; ; i++)
            {
                for (int j = 0; ; j++)
                {
                    if (j == needle.Length) return i;
                    if (i + j == haystack.Length) return -1;
                    if (needle[j] != haystack[i + j]) break;
                }
            }
        }

    }
}
