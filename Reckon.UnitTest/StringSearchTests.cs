﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Reckon.Test
{
    [TestClass]
    public class StringSearchTests
    {
        string stringToSearch = "Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out. Phew!";

        [TestMethod]
        public void SearchString_Findt_Method_Returns_Index_SearchFor_Peter()
        {
            // Arrange
            var subText = "Peter";
            var expected = "1, 43, 98";

            // Act
            var actual = SearchString.Find(stringToSearch, subText);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SearchString_Findt_Method_Returns_Index_SearchFor_Pi()
        {
            // Arrange
            var subText = "Pi";
            var expected = "53, 60, 66, 74, 81";

            // Act
            var actual = SearchString.Find(stringToSearch, subText);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SearchString_Findt_Method_Returns_Index_SearchFor_Z()
        {
            // Arrange
            var subText = "Z";
            var expected = "<No Output>";

            // Act
            var actual = SearchString.Find(stringToSearch, subText);

            // Assert
            Assert.AreEqual(expected, actual);
        }


    }
}
