﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Reckon.Test
{
    [TestClass]
    public class BossHogTests
    {

        [TestMethod]
        public void BossHog_GetOutput_Method_Returns_Boss_When_Number_Is_Divisible_By_Only_3()
        {
            // Arrange
            var expected = "Boss";

            // Act
            var actual = BossHog.GetValue(3);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BossHog_GetOutput_Method_Returns_Hog_When_Number_Is_Divisible_By_Only_5()
        {
            // Arrange
            var expected = "Hog";

            // Act
            var actual = BossHog.GetValue(5);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BossHog_GetOutput_Method_Returns_BossHog_When_Number_Is_Divisible_By_3_And_5()
        {  
            // Arrange
            var expected = "BossHog";

            // Act
            var actual = BossHog.GetValue(15);

            // Assert
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod]
        public void BossHog_GetOutput_Method_Returns_Number_When_Number_Is_Not_Divisible_By_3_Or_5()
        {

            // Arrange
            var expected = "17";

            // Act
            var actual = BossHog.GetValue(17);

            // Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
